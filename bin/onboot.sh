#!/bin/bash
########################################
## onboot
## - A run once script to start on splunkd boot
##   which removes the restart state file
##---------------------
## Author: Chris Barbour
## - email: chris.barbour@katana1.com
########################################

# State file
STATE_FILE_PATH="var/restarting.state"

# Get the directory containing this script (<app>/bin/onboot.sh)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Remove the file located without our apps' var dir
rm ${DIR}/../${STATE_FILE_PATH}
