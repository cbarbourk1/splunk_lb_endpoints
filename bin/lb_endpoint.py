import os
import sys
import logging

DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, DIR)
import rest_handler


# Implement `touch` in Python
def touch(fname, times=None):
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, times)
        return (True, None)
    except IOError as e:
        return (False, e)
    finally:
        fhandle.close()
    

class LBHandler(rest_handler.RESTHandler):
    def __init__(self, command_line, command_arg):
        logger = logging.getLogger('LBHandler')
        logger.setLevel(logging.INFO)
        super(LBHandler, self).__init__(command_line, command_arg, logger)
        

    def get_ping(self, request_info, **kwargs):
        """
        Return a response indicating that the REST handler is online.
        """
        if os.path.isfile(self.statefile):
            # We are restarting
            return {
                'payload': 'Restarting',
                'status': 503,
                'headers': {
                    'Content-Type': 'text/plain'
                },
            }
        else:
            return {
                'payload': 'Online',
                'status': 200,
                'headers': {
                    'Content-Type': 'text/plain'
                },
            }

    def get_restart(self, request_info, **kwargs):
        """
        Return a response indicating that the REST handler is online.
        """
        res = touch(self.statefile)
        if os.path.isfile(self.statefile):
            # We are restarting
            return {
                'payload': 'Restarting',
                'status': 200,
                'headers': {
                    'Content-Type': 'text/plain'
                },
            }
        else:
            return {
                'payload': 'Unable to create state file: {}'.format(res[1]),
                'status': 500,
                'headers': {
                    'Content-Type': 'text/plain'
                },
            }
