# Splunk Load Balancer Endpoints

Super basic app with provides two REST endpoints for Splunk, ping and restart.
The app checks if you have signaled for a restart or initiates a restart using a "state" file (created at `<app_dir>/var/restarting.state`).

> Note: when splunk starts the `onboot.sh` script will be run (once) which removes the `restarting.state` file

## `/lb_endpoint/`
## `/ping`

```bash
curl -k -u 'admin' "https://127.0.0.1:8089/services/lb_endpoint/ping"
```

To be used by a Load Balancer to check if Splunk is running and not about to restart by checking a state file. See below for expected output:

#### Success

> status: 200

> content: Online

```bash
> GET /services/lb_endpoint/ping HTTP/1.1
> Host: 127.0.0.1:8089
> Authorization: Basic YWRtaW46ITcyOXMqMkB2U3lrJmR4aA==
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Wed, 12 Feb 2020 09:31:58 GMT
< Expires: Thu, 26 Oct 1978 00:00:00 GMT
< Cache-Control: no-store, no-cache, must-revalidate, max-age=0
< Content-Type: text/plain
< X-Content-Type-Options: nosniff
< Content-Length: 6
< Vary: *
< Connection: Keep-Alive
< X-Frame-Options: SAMEORIGIN
< Server: Splunkd
< 
* Connection #0 to host 127.0.0.1 left intact
Online
```

#### Restarting

> status: 503

> content: Restarting

```bash
> GET /services/lb_endpoint/ping HTTP/1.1
> Host: 127.0.0.1:8089
> Authorization: Basic YWRtaW46ITcyOXMqMkB2U3lrJmR4aA==
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 503 Service Unavailable
< Date: Wed, 12 Feb 2020 09:36:29 GMT
< Expires: Thu, 26 Oct 1978 00:00:00 GMT
< Cache-Control: no-store, no-cache, must-revalidate, max-age=0
< Content-Type: text/plain
< X-Content-Type-Options: nosniff
< Content-Length: 10
< Vary: *
< Connection: Keep-Alive
< X-Frame-Options: SAMEORIGIN
< Server: Splunkd
< 
* Connection #0 to host 127.0.0.1 left intact
Restarting
```

## `/restart`

```bash
curl -k -u 'admin' "https://127.0.0.1:8089/services/lb_endpoint/restart"
```

To be used by automation to set the state to `restarting` (creates a 'state' file at `<app_dir>/var/restarting.state`). See below for expected output:

#### Success

> status: 200

> content: Restarting

```bash
> GET /services/lb_endpoint/restart HTTP/1.1
> Host: 127.0.0.1:8089
> Authorization: Basic YWRtaW46ITcyOXMqMkB2U3lrJmR4aA==
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Wed, 12 Feb 2020 09:32:52 GMT
< Expires: Thu, 26 Oct 1978 00:00:00 GMT
< Cache-Control: no-store, no-cache, must-revalidate, max-age=0
< Content-Type: text/plain
< X-Content-Type-Options: nosniff
< Content-Length: 10
< Vary: *
< Connection: Keep-Alive
< X-Frame-Options: SAMEORIGIN
< Server: Splunkd
< 
* Connection #0 to host 127.0.0.1 left intact
Restarting
```
